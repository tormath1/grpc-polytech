### Welcome aboard

In this `README`, I'm going to explain how to use this examples.

#### Server

The main server is developed with `java` and managed throught `maven`. Before starting, please make sure that this tools are installed and `java>=1.8`.

If everything is alright, you should have something like that:

```shell
$ mvn --version
Apache Maven 3.6.0 (NON-CANONICAL_2018-11-06T03:14:22+01:00_root; 2018-11-06T03:14:22+01:00)
Maven home: /opt/maven
Java version: 1.8.0_192, vendor: Oracle Corporation, runtime: /usr/lib/jvm/java-8-openjdk/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.19.2-arch1-1-arch", arch: "amd64", family: "unix"
```

Please take a loot at the current folders tree: 

```shell
$ tree . 
.
├── pom.xml
└── src
    └── main
        ├── java
        │   └── org
        │       └── local
        │           └── tld
        │               └── App.java
        └── proto
            └── api.proto

7 directories, 3 files
```

`api.proto` is our proto file, this is where we describe our apis. He must be placed in to `src/main/proto` folder.

Now, everything is ready in order to package our application.

```shell
$ mvn package
```

will create a `*.jar` file in the `target/` folder, to run this application, juste launch this command: 

```shell
$ mvn exec:java -Dexec.mainClass=org.local.tld.App
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Detecting the operating system and CPU architecture
[INFO] ------------------------------------------------------------------------
[INFO] os.detected.name: linux
[INFO] os.detected.arch: x86_64
[INFO] os.detected.version: 4.19
[INFO] os.detected.version.major: 4
[INFO] os.detected.version.minor: 19
[INFO] os.detected.release: arch
[INFO] os.detected.release.like.arch: true
[INFO] os.detected.release.like.archlinux: true
[INFO] os.detected.classifier: linux-x86_64
[INFO] 
[INFO] ------------------------< org.local.tld:server >------------------------
[INFO] Building server 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ server ---
Nov 28, 2018 2:13:45 PM org.local.tld.App start
INFO: server running on : localhost:50051
```

The last line is the most important: our server is running :smile: 

#### Client

Our client is developed with Python. The setup is easier: 

First, you need to have `python3.x`, `virtualenv` and `pip` on your machine. 

```shell
$ python --version
Python 3.7.1
$ virtualenv --version
16.0.0
$ pip --version
pip 18.1 from /home/mathieu/gitlab/grpc-samir/client/venv/lib/python3.7/site-packages/pip (python 3.7)
```

Now, we can create a `virtualenv` in order to not pollute our main environment: 

```shell
$ cd client
$ virtualenv venv
$ source venv/bin/activate
(venv) $
```

*NB*: If you want to leave this environment, you can simply run `(venv)$ deactivate`.

We are ready to install our missing dependencies, thanksfully, the requirements are stored in a file, so just run this command: 

```shell
(venv)$ pip install -r requirements.txt
```

Like with JAVA; we need to generate our python file from `api.proto`.


```shell
$ python -m grpc_tools.protoc -I ${PWD}/proto api.proto --python_out=${PWD} --grpc_python_out=${PWD}
```

Now, 2 files are appeared, so we can run our `client.py` script. This script implements a function from one of our generated files. This is why this step is mandatory. 

To run this script, simply launch this command: 

```shell
(venv)$ python client.py toto
message: "Hello toto"
```

So everything is working ! Congrats, you've just run your first gRPC api !! 