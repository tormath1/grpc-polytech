from sys import argv

import grpc

import api_pb2
import api_pb2_grpc

class Client(object):

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.channel = grpc.insecure_channel(
            "{}:{}".format(self.host, self.port)
        )

        self.stub = api_pb2_grpc.GreeterStub(self.channel)

    def say_hello(self, name):
        to_digest_message = api_pb2.HelloRequest(name=name)
        return self.stub.SayHello(to_digest_message)

if __name__ == "__main__":
    
    if len(argv) < 2:
        print("usage: python client.py <your-name>")
        exit(0)

    client = Client("localhost", "50051")
    print(client.say_hello(name=argv[1]))